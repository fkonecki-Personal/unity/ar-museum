using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace ARMuseum
{
    [CreateAssetMenu(fileName = "ARManager", menuName = "Managers/ARManager")]
    public class ARManager : ScriptableObject
    {
        string _description;
        string _shortDescription;

        public string GetDescription() 
        {
            return _description;
        }

        public string GetShortDescription()
        {
            return _shortDescription;
        }

        public void LoadDescription(
            AssetReferenceT<TextAsset> descriptionRef,
            AssetReferenceT<TextAsset> shortDescriptionRef)
        {
            descriptionRef.LoadAssetAsync().Completed += DescriptionLoader;
            shortDescriptionRef.LoadAssetAsync().Completed += ShortDescriptionLoader;

            descriptionRef.ReleaseAsset();
            shortDescriptionRef.ReleaseAsset();
        }

        private void DescriptionLoader(AsyncOperationHandle<TextAsset> obj)
        {
            switch (obj.Status)
            {
                case AsyncOperationStatus.Succeeded:
                    TextAsset descriptionAsset = obj.Result;
                    _description = descriptionAsset.text;
                    break;
                case AsyncOperationStatus.Failed:
                    Debug.LogError("Questions load failed.");
                    break;
                default:
                    // case AsyncOperationStatus.None:
                    break;
            }
        }

        private void ShortDescriptionLoader(AsyncOperationHandle<TextAsset> obj)
        {
            switch (obj.Status)
            {
                case AsyncOperationStatus.Succeeded:
                    TextAsset asset = obj.Result;
                    _shortDescription = asset.text;
                    break;
                case AsyncOperationStatus.Failed:
                    Debug.LogError("Questions load failed.");
                    break;
                default:
                    // case AsyncOperationStatus.None:
                    break;
            }
        }
    }
}
