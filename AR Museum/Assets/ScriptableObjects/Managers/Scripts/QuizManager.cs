using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace ARMuseum
{
    [CreateAssetMenu(fileName = "QuizManager", menuName = "Managers/QuizManager")]
    public class QuizManager : ScriptableObject
    {
        [SerializeField] GameEvent _loadNextQnA;

        QuestionList _questions;

        List<int> unansweredQuestionIndexes;
        int _currentQuestionIndex;
        bool _isCurrentQuestionAnswered;
        int _correctCount;
        int _incorrectCount;
        bool _isQuizOver;

        public void LoadQuestions(AssetReferenceT<TextAsset> questionsJsonRef) {
            Init();
            _questions = null;

            questionsJsonRef.LoadAssetAsync().Completed += AssetLoader;
            questionsJsonRef.ReleaseAsset();
        }

        public void Reset()
        {
            Init();
            _loadNextQnA.TriggerEvent();
        }

        public void Init()
        {
            unansweredQuestionIndexes = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            _currentQuestionIndex = Random.Range(0, 10);
            _isCurrentQuestionAnswered = false;
            _correctCount = 0;
            _incorrectCount = 0;
            _isQuizOver = false;
        }

        private void AssetLoader(AsyncOperationHandle<TextAsset> obj)
        {
            switch (obj.Status)
            {
                case AsyncOperationStatus.Succeeded:
                    TextAsset questionsJson = obj.Result;
                    _questions = JsonUtility.FromJson<QuestionList>(questionsJson.text);
                    break;
                case AsyncOperationStatus.Failed:
                    Debug.LogError("Questions load failed.");
                    break;
                default:
                    // case AsyncOperationStatus.None:
                    break;
            }
        }

        public string GetQuestion()
        {
            return !_isQuizOver ? _questions._questions[_currentQuestionIndex]._questionText : GetStatistic();
        }

        public string GetAnswer(int index)
        {
            return !_isQuizOver ? _questions._questions[_currentQuestionIndex]._answers[index] : "";
        }

        string GetStatistic() 
        {
            return _correctCount + "/10";
        }

        public void Answer(int answer)
        {
            if (!_isCurrentQuestionAnswered)
            {
                _isCurrentQuestionAnswered = true;
                if (answer == _questions._questions[_currentQuestionIndex]._correctAnswerIndex)
                    _correctCount++;
                else
                    _incorrectCount++;

                NextQuestion();
            }
        }

        public void NextQuestion()
        {
            if (unansweredQuestionIndexes.Count > 1)
            {
                unansweredQuestionIndexes.Remove(_currentQuestionIndex);
                _currentQuestionIndex = unansweredQuestionIndexes[Random.Range(0, unansweredQuestionIndexes.Count)];
                _isCurrentQuestionAnswered = false;
            }
            else 
            {
                _isQuizOver = true;
            }

            _loadNextQnA.TriggerEvent();
        }
    }
}