using UnityEngine;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

namespace ARMuseum
{
    [CreateAssetMenu(fileName = "GameManager", menuName = "Managers/GameManager")]
    public class GameManager : ScriptableObject
    {
        bool _isLanguageEng = true;

        [SerializeField] AssetReferenceT<TextAsset> _qnaEng;
        [SerializeField] AssetReferenceT<TextAsset> _qnaHrv;

        [SerializeField] AssetReferenceT<TextAsset> _iguanaDescEng;
        [SerializeField] AssetReferenceT<TextAsset> _iguanaDescHrv;
        [SerializeField] AssetReferenceT<TextAsset> _iguanaShortDescEng;
        [SerializeField] AssetReferenceT<TextAsset> _iguanaShortDescHrv;

        [SerializeField] AssetReferenceT<TextAsset> _whaleDescEng;
        [SerializeField] AssetReferenceT<TextAsset> _whaleDescHrv;
        [SerializeField] AssetReferenceT<TextAsset> _whaleShortDescEng;
        [SerializeField] AssetReferenceT<TextAsset> _whaleShortDescHrv;

        [SerializeField] QuizManager _quizManager;
        [SerializeField] ARManager _arManager;

        [SerializeField] GameEvent _languageSwitch;

        readonly Dictionary<string, List<string>> LOCALE_TABLE =
            new Dictionary<string, List<string>>() {
                {"Quiz", new List<string>(){ "Quiz", "Kviz"} },
                {"Exit", new List<string>(){ "Exit", "Izlaz"} },
                {"Menu", new List<string>(){ "Main Menu", "Izbornik"} },
                {"Start", new List<string>(){ "Start/Retry", "Pokreni/Ponovi"} },
                {"Map", new List<string>(){ "Map", "Mapa"} },
                {"Desc", new List<string>(){ "Description", "Opis"} },
                {"ShortDesc", new List<string>(){ "Short Description", "Kratki opis"} }
            };

        // Locale **************************************
        public void SetLanguageEng()
        {
            _isLanguageEng = true;
            _languageSwitch.TriggerEvent();
        }

        public void SetLanguageHrv()
        {
            _isLanguageEng = false;
            _languageSwitch.TriggerEvent();
        }

        public bool GetIsLanguageEng()
        {
            return _isLanguageEng;
        }

        public string GetLocale(string key)
        {
            if (LOCALE_TABLE.ContainsKey(key))
                return LOCALE_TABLE[key][_isLanguageEng ? 0 : 1];
            else
                return "MISSING_LOCALE _KEY";
        }

        // AR Info ***********************************
        public void LoadIguanaInfo()
        {
            _arManager.LoadDescription(GetIguanaDesc(), GetIguanaShortDesc());
        }

        public void LoadWhaleInfo()
        {
            _arManager.LoadDescription(GetWhaleDesc(), GetWhaleShortDesc());
        }

        public AssetReferenceT<TextAsset> GetIguanaDesc()
        {
            return _isLanguageEng ? _iguanaDescEng : _iguanaDescHrv;
        }

        public AssetReferenceT<TextAsset> GetIguanaShortDesc()
        {
            return _isLanguageEng ? _iguanaShortDescEng : _iguanaShortDescHrv;
        }

        public AssetReferenceT<TextAsset> GetWhaleDesc()
        {
            return _isLanguageEng ? _whaleDescEng : _whaleDescHrv;
        }

        public AssetReferenceT<TextAsset> GetWhaleShortDesc()
        {
            return _isLanguageEng ? _whaleShortDescEng : _whaleShortDescHrv;
        }

        // Scenes *****************************************

        public void SwitchSceneToMainMenu() {
            SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
        }

        public void SwitchSceneToQuiz()
        {
            SceneManager.LoadScene("Quiz", LoadSceneMode.Single);
            _quizManager.LoadQuestions(GetQuizQnA());
        }

        public void SwitchSceneToAR()
        {
            SceneManager.LoadScene("AR Museum", LoadSceneMode.Single);
        }




        public AssetReferenceT<TextAsset> GetQuizQnA()
        {
            return _isLanguageEng ? _qnaEng : _qnaHrv;
        }

        public void Exit()
        { 
            Application.Quit();
        }

    }
}
