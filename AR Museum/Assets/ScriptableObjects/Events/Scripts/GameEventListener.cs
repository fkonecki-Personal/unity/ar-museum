﻿using UnityEngine;
using UnityEngine.Events;

namespace ARMuseum
{
    public class GameEventListener : MonoBehaviour
    {
        [SerializeField] private GameEvent _event;
        [SerializeField] private UnityEvent _action;

        void OnEnable()
        {
            _event.AddListener(this);
        }
        void OnDisable()
        {
            _event.RemoveListener(this);
        }
        public void OnEventTriggered()
        {
            _action.Invoke();
        }
    }
}