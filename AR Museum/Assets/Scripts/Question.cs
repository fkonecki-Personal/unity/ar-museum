﻿namespace ARMuseum
{
    [System.Serializable]
    public class Question
    {
        public string _questionText;
        public string[] _answers;
        public int _correctAnswerIndex;
    }
}