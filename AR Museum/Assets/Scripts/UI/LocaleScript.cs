using UnityEngine;

namespace ARMuseum
{
    public class LocaleScript : GameEventListener
    {

        [SerializeField] string _localeKey;
        [SerializeField] GameManager _gameManager;

        void Start()
        {
            Load();
        }

        public void Load()
        {
            GetComponent<UnityEngine.UI.Text>().text =
                _gameManager.GetLocale(_localeKey);
        }

    }
}
