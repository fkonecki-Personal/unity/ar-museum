using UnityEngine;

namespace ARMuseum
{
    public class QuestionBoxScript : MonoBehaviour
    {
        [SerializeField] QuizManager _quizManager;
        [SerializeField] UnityEngine.UI.Text _textComponent;

        public void Load()
        {
            _textComponent.text = _quizManager.GetQuestion();
        }
    }
}
