using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARMuseum {
    public class ARInfoScript : MonoBehaviour
    {
        [SerializeField] GameObject _infoBox;
        GameObject _spawnedInfo;

        [SerializeField] GameObject _infoPrefab;
        [SerializeField] ARManager _arManager;

        [SerializeField] GameObject _prefab;
        [SerializeField] GameObject _spawnParent;

        [SerializeField] Sprite _iguanaMap;
        [SerializeField] Sprite _whaleMap;

        GameObject _spawnedObject;

        [SerializeField] GameManager _gameManager;

        public void SpawnIguana() 
        {
            _gameManager.LoadIguanaInfo();
            SetPrefabImage(_iguanaMap);
            Spawn();
        }

        public void SpawnWhale()
        {
            _gameManager.LoadWhaleInfo();
            SetPrefabImage(_whaleMap);
            Spawn();
        }

        void SetPrefabImage(Sprite image) {
            _infoPrefab.transform
                    .Find("Map")
                    .Find("MapImage")
                    .GetComponent<UnityEngine.UI.Image>().sprite = image;
        }

        void Spawn()
        {
            GetDescriptions();

            _spawnedInfo = Instantiate(_infoPrefab, Vector3.zero, Quaternion.identity);
            _spawnedInfo.transform.SetParent(_infoBox.transform, false);

            if (_spawnedObject) Destroy(_spawnedObject);

            _spawnedObject = Instantiate(_prefab, _prefab.transform);
            _spawnedObject.transform.SetParent(_spawnParent.transform);
            _spawnedObject.transform.localPosition = Vector3.zero;
        }

        public void DestroySpawned()
        {
            Destroy(_spawnedObject);
            Destroy(_spawnedInfo);
            _spawnedObject = null;
        }

        private void GetDescriptions() 
        {
            _infoPrefab.transform
                    .Find("Description")
                    .Find("DescriptionText")
                    .GetComponent<UnityEngine.UI.Text>().text = _arManager.GetDescription();

            _infoPrefab.transform
                .Find("ShortDescription")
                .Find("ShortDescriptionText")
                .GetComponent<UnityEngine.UI.Text>().text = _arManager.GetShortDescription();
        }

    }
}
