using System.Collections.Generic;
using UnityEngine;

namespace ARMuseum
{
    public class OnInactiveDisableOthersScript : MonoBehaviour
    {
        [SerializeField] List<GameObject> _toggleObjects = new List<GameObject>();
        [SerializeField] List<GameObject> _infoObjects = new List<GameObject>();

        readonly List<UnityEngine.UI.Toggle> _toggleComponents = new List<UnityEngine.UI.Toggle>();

        void Start()
        {
            foreach (GameObject obj in _toggleObjects)
                _toggleComponents.Add(obj.GetComponent<UnityEngine.UI.Toggle>());
        }

        void OnDisable()
        {
            HideAll(_infoObjects);
            HideAll(_toggleObjects);
        }

        void OnEnable()
        {
            GetComponent<UnityEngine.UI.Toggle>().isOn = false;

            DisableToggles();
            HideAll(_infoObjects);
        }

        void HideAll(List<GameObject> toHide) {
            foreach (GameObject obj in toHide)
                obj.SetActive(false);
        }

        public void DisableToggles() {
            foreach (UnityEngine.UI.Toggle toggle in _toggleComponents)
                toggle.isOn = false;
        }

    }
}
