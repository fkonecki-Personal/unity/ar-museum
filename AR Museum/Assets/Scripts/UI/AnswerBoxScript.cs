using UnityEngine;
using UnityEngine.EventSystems;

namespace ARMuseum
{
    public class AnswerBoxScript : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] QuizManager _quizManager;
        [SerializeField] int _answerIndex;
		UnityEngine.UI.Text _textComponent;

        void Start() {
            _textComponent = GetComponent<UnityEngine.UI.Text>();
            Load();
        }

        public void Load()
        {
          _textComponent.text = _quizManager.GetAnswer(_answerIndex);
        }

		public void OnPointerClick(PointerEventData eventData)
		{
			_quizManager.Answer(_answerIndex);
		}
	}
}
