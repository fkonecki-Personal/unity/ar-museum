using UnityEngine;

namespace ARMuseum {
    public class QnAScript : MonoBehaviour
    {
        [SerializeField] QuizManager _quizManager;
        [SerializeField] GameObject _qnaBoxPrefab;
        GameObject qna;

        void Start()
        {
            _quizManager.Init();
        }

        public void OnNextQuestion() {
            Destroy(qna);
            SpawnQnABox();
        }

        public void SpawnQnABox() {
            _qnaBoxPrefab.transform.Find("Question").GetComponent<UnityEngine.UI.Text>().text =
                _quizManager.GetQuestion();

            _qnaBoxPrefab.transform.Find("Answer1").Find("Text")
                .GetComponent<UnityEngine.UI.Text>().text =
                _quizManager.GetAnswer(0);
            _qnaBoxPrefab.transform.Find("Answer2").Find("Text")
                .GetComponent<UnityEngine.UI.Text>().text =
                _quizManager.GetAnswer(1);
            _qnaBoxPrefab.transform.Find("Answer3").Find("Text")
                .GetComponent<UnityEngine.UI.Text>().text =
                _quizManager.GetAnswer(2);
            _qnaBoxPrefab.transform.Find("Answer4").Find("Text")
                .GetComponent<UnityEngine.UI.Text>().text =
                _quizManager.GetAnswer(3);

            qna = Instantiate(_qnaBoxPrefab, Vector3.zero, Quaternion.identity);
            qna.transform.SetParent(gameObject.transform, false);
        }
    }
}
