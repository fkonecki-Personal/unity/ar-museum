# AR Museum

<img src="/Screenshots/Main.png" alt="Screenshot" width="900" height="450">

Application has: 
- Main Menu
- Quiz
- AR Experience

## Main Menu
- Splash screen when opening the app
- Options to open quiz or AR experience
- Language select
- Exit button

## Quiz
- Has 10 random questions about animals
- Questions are fetchef from JSON formated file
- When quiz is over, result statistics are displayed

## AR Experience
- When marker is scanned (Vuforia), 3D model of the animal is displayed with 3 buttons for displaying short info, long info and map
